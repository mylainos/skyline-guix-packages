(define-module (skyline skyline-website)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system trivial)
  #:use-module (guix licenses))

(define %version "0.6")
(define %hash "0mkskf8ihbdi96s4yf85b0is2bq95z4yrj0wnyj9pcw7alwmhf13")

(define-public skyline-website
  (package
   (name "skyline-website")
   (version %version)
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://framagit.org/mylainos/skyline-website.git")
                  (commit (string-append "v" version))))
            (sha256
             (base32 %hash))))
   (build-system trivial-build-system)
   (arguments
    '(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let ((target (assoc-ref %outputs "out"))
              (source (assoc-ref %build-inputs "source")))
          (mkdir target)
          (copy-recursively source target)))))
   (synopsis "The roger skyline website")
   (description "The best website in the world")
   (home-page "https://framagit.org/mylainos/skyline-website")
   (license gpl3+)))
